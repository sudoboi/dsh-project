import os

# Numpy functions
import numpy
import numpy as np
from numpy import expand_dims
from numpy import zeros
from numpy import ones
from numpy import vstack
from numpy.random import randn
from numpy.random import randint
from numpy import zeros
from numpy import ones
from numpy import asarray

#Torchvision for fast and easy loading and resizing
import torchvision
import torchvision.transforms as transforms

from PIL import Image

from IPython import display
import matplotlib.pyplot as plt
import random
from tensorflow.keras import mixed_precision

# Tensorflow/Keras functions
import tensorflow as tf

from keras.optimizers import Adam
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Conv2D, Conv2DTranspose, Reshape, Flatten
from keras.layers import Dropout, LeakyReLU, BatchNormalization
from keras.layers import Activation, ZeroPadding2D, UpSampling2D
from keras.layers import Input, Reshape
from matplotlib import pyplot
from IPython.display import clear_output



# --------------- CONFIGURE THE GPU FOR CUDA AND TENSORFLOW -------------------#
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_global_policy(policy)

config = tf.compat.v1.ConfigProto(gpu_options =
                         tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.7)
# device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(session)
# ----------------------------------------------------------------------------#

#------------------------- START OF GAN --------------------------------------#
BATCH_SIZE = 32

def load_Dataset():
    dataset_path = "../Faces_Dataset_v2"
    #saved_path = "training_data.npy"
    saved_path = "training_data_2.npy"
    image_width = 64
    image_height = 64
    image_size = (image_height,image_width)

    # Search the directory for the file already existing
    print("Looking for previously saved file..")

    if not os.path.isfile(saved_path):
        print("\n File not found, creating new one")
        train_ds = []
        # Define the image transformation (64 * 64)
        transform_ds = transforms.Compose([transforms.Resize(image_size),])
        # Directory containing the transformed images
        image_folder = torchvision.datasets.ImageFolder(root=dataset_path,transform=transform_ds)
        print(f"Found {len(image_folder)} artistic portraits")
        print("Converting images...")
        for i in range(len(image_folder)):
            # Retrieve the numpy.array of each image
            image_array = numpy.array(image_folder[i][0])
            # Store the array representation in the training dataset for training the GAN
            train_ds.append(image_array)
            if i % 500 == 0:
                print(f"Processed {i} pictures")
        print("Finished Converting images")

        # Convert training dataset to float 32
        train_ds = np.array(train_ds,dtype=np.float32)
        # Normalize training set for training
        train_ds = (train_ds - 127.5) / 127.5
        np.save(saved_path,train_ds)
    else:
        print("Found the data")
        train_ds = np.load(saved_path)

    # Shuffle the training dataset and split into batches of size 32
    train_ds = tf.data.Dataset.from_tensor_slices(train_ds).shuffle(9000).batch(32)

    return train_ds

# Build the generator
def build_generator(seed_size, channels):
    model = Sequential()

    model.add(Dense(64*64, activation="relu", input_dim=seed_size))  # 64x64 units
    model.add(Reshape((4, 4, 256)))

    model.add(UpSampling2D())
    model.add(Conv2D(256, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))

    model.add(UpSampling2D())
    model.add(Conv2D(256, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))

    model.add(Conv2D(256, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))

    # Optional additional upsampling goes here
    model.add(UpSampling2D(size=(2,2)))  # 4,4 for 128x128, 2,2 for 64x64
    model.add(Conv2D(256, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))

    model.add(UpSampling2D(size=(2,2)))
    model.add(Conv2D(256, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))

    model.add(Conv2D(channels, kernel_size=3, padding="same"))
    model.add(Activation("tanh"))

    return model


# Build the discriminator to fight the generator
def build_discriminator(image_shape):
    model = Sequential()

    model.add(tf.keras.layers.experimental.preprocessing.Rescaling(1./255))

    model.add(Conv2D(32, kernel_size=3, strides=2, input_shape=image_shape,
                     padding="same"))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Dropout(0.25))
    model.add(Conv2D(64, kernel_size=3, strides=2, padding="same"))
    model.add(ZeroPadding2D(padding=((0, 1), (0, 1))))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Dropout(0.25))
    model.add(Conv2D(128, kernel_size=3, strides=2, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Dropout(0.25))
    model.add(Conv2D(256, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Dropout(0.25))
    model.add(Conv2D(512, kernel_size=3, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU(alpha=0.2))

    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(1, activation='sigmoid'))

    return model

# ------------------- GLOBAL VARIABLES ----------------------#
# Size of the seed array
seed_size = 100

# Build the generator and the discriminator
generator = build_generator(seed_size, 3)
discriminator = build_discriminator((64, 64,3))

# Define the noise, create a fake image with the generator and plot it
noise = tf.random.normal([1, seed_size])
fake_img = generator(noise, training=False)
print("fake image: ", fake_img.shape)

# Print the decision of the discriminator
decision = discriminator(fake_img)
print("decision: ", decision)

# Define the cross_entropy function
cross_entropy = tf.keras.losses.BinaryCrossentropy()

# Define the generator and discriminator optimizers
generator_optimizer = tf.keras.optimizers.Adam()
discriminator_optimizer = tf.keras.optimizers.Adam(0.0002,0.5)

# Define the seed to see the evolution of our images (big_seed)
big_seed = tf.random.normal([49,seed_size])

# ---------------------------------------------------------------------------#

# Define the discriminator loss
def discriminator_loss(real_output, fake_output):
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss

# Define the generator loss
def generator_loss(fake_output):
    return 2 * cross_entropy(tf.ones_like(fake_output), fake_output)


# Define the training function
def train_step(images):

    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
        seed = tf.random.normal([BATCH_SIZE, seed_size])
        generated_images = generator(seed, training=True)

        real_output = discriminator(images, training=True)
        fake_output = discriminator(generated_images, training=True)

        gen_loss = generator_loss(fake_output)
        disc_loss = discriminator_loss(real_output, fake_output)

        gradients_of_generator = gen_tape.gradient(
            gen_loss,
            generator.trainable_variables
        )
        gradients_of_discriminator = disc_tape.gradient(
            disc_loss,
            discriminator.trainable_variables
        )

        generator_optimizer.apply_gradients(zip(gradients_of_generator,generator.trainable_variables))

        discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator,discriminator.trainable_variables))

        return gen_loss, disc_loss

# -------------- GLOBAL VARIABLES ---------------------#
GENERATED_ROWS = 7
GENERATED_COLS = 7
MARGIN = 2 # Give the images a little frame


def generate_and_save_images(generator,image_seed,cnt):
    # Define the "base" of the saved image as a big black canvas
    image_array = np.full((
        MARGIN + (GENERATED_ROWS * (64 + MARGIN)),
        MARGIN + (GENERATED_COLS * (64 + MARGIN)), 3),
        0, dtype=np.uint8)

    generated_images = generator.predict(image_seed)

    image_count = 0
    for row in range(GENERATED_ROWS):
        for col in range(GENERATED_COLS):
            r = row * (64 + 2) + MARGIN
            c = col * (64 + 2) + MARGIN
            image_array[r:r + 64, c:c + 64] = generated_images[image_count] * 127.5 + 127.5
            image_count += 1

    output_path = "Generated"
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    filename = os.path.join(output_path, f"train-{str(cnt).zfill(4)}.png")
    im = Image.fromarray(image_array)
    im.save(filename)

def train(dataset, epochs):
    # Use a fixed seed for the saved images so we can watch their development
    fixed_seed = big_seed

    for epoch in range(epochs):
        gen_loss_list = []
        disc_loss_list = []

        for image_batch in dataset:
            t = train_step(image_batch)
            gen_loss_list.append(t[0])
            disc_loss_list.append(t[1])

        #display.clear_output(wait=True)
        # Generate and save the images from training produced at each epoch
        if (epoch + 1 ) % 10 == 0:
            generate_and_save_images(generator=generator,image_seed= fixed_seed,cnt=epoch + 1)

        g_loss = sum(gen_loss_list) / len(gen_loss_list)  # calculate losses
        d_loss = sum(disc_loss_list) / len(disc_loss_list)

        print(f'Epoch {epoch + 1}, gen loss = {g_loss}, disc loss = {d_loss}')


def test_model(generator = None, discriminator = None):
    print("Testing")
    generator = tf.keras.models.load_model('generator.h5')
    seed2 = tf.random.normal([32, seed_size])

    for i in range(30):
        generated_images2 = np.asarray(generator(seed2, training=True)[i,:,:,:], dtype=np.float32)
        generated_images2 = 0.5 * generated_images2 + 0.5
        plt.imshow(generated_images2)
        plt.show()


def main():
    val = input("train (1) or test (2)?")

    if val == "1":
        # Load the dataset
        training_dataset = load_Dataset()
        # Train the GAN
        train(dataset=training_dataset, epochs=500)
        #Save the generator and discriminator
        generator.save('generator.h5')
        discriminator.save('discriminator.h5')

    elif val == "2":
        #Test the model
        test_model()

if __name__ == "__main__":
    main()
