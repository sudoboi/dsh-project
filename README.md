# dsh-project

Repository for the project of Artist Style Recognition in Portraits. Done within the course of Data Science for Humanities.

## Environment setup

1. Enter a virtual environment of your choice
2. Run <code>python -m pip install -r requirements.txt</code>

## Usage

1. python3 src/CNN-\<preferred network\> --> Correspond to the CNN implementations for the project
2. python3 Gan_models/GAN-from-scratch.py --> Corresponds to the GAN implementation for the project
3. python3 Gan_models -\<prefered model\> --> Correspond to the saved model files (discriminator or generator) for the GAN
