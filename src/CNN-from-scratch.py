
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# Imports
import glob
import os
import time

import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.callbacks import Callback, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras import layers
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn import metrics


def train_model(dataset,epochs):

    # Model Parameters
    train_input_shape = (256,256,3)
    train_ds = dataset[0]
    val_ds = dataset[1]

    # Define the classification names
    class_names = train_ds.class_names
    print(class_names)

    # Store the datasets into the GPU mem cache
    AUTOTUNE = tf.data.AUTOTUNE
    train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

    # Define Model architecture
    model = tf.keras.Sequential([
        # layers.experimental.preprocessing.Rescaling(1./255),
        layers.Conv2D(32, 3, activation='relu'),
        layers.AveragePooling2D(),
        layers.Conv2D(32, 3, activation='relu'),
        layers.AveragePooling2D(),
        layers.Conv2D(32, 3, activation='relu'),
        layers.GlobalMaxPooling2D(),
        layers.Flatten(),
        layers.Dense(256, activation='relu'),
        layers.Dense(len(class_names), activation='softmax')
    ])

    # Compile the model
    model.compile(
        optimizer='adam',
        loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=['accuracy'])

    # Callback to reduce LR (not used here yet)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5,
                                  verbose=1, mode='auto')

    # Start time
    start = time.time()

    # Train the model and store the history
    history = model.fit(train_ds,
                        validation_data=val_ds,
                        epochs=epochs,
                        shuffle=True
    )

    # End Time
    end = time.time()
    print("Total training time: ", end - start)


    # Plot the history of the training
    plt.figure(figsize=(10, 10))

    plt.subplot(2, 2, 1)
    plt.plot(history.history['loss'], label='Loss')
    plt.plot(history.history['val_loss'], label='Validation Loss')
    plt.legend()
    plt.title('Training - Loss Function')

    plt.subplot(2, 2, 2)
    plt.plot(history.history['accuracy'], label='Accuracy')
    plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
    plt.legend()
    plt.title('Train - Accuracy')

    # Prediction accuracy on train data
    score = model.evaluate(train_ds, verbose=1)
    print("Prediction accuracy on train data =", score[1])

    score = model.evaluate(val_ds, verbose=1)
    print("Prediction accuracy on test data =", score[1])

    plt.show()

    return model


def load_dataset(batch_size):
    # Training dataset
    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        '../AF_dataset',
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(256, 256),
        batch_size=batch_size
    )

    # Validation dataset
    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        '../AF_dataset',
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(256, 256),
        batch_size=batch_size
    )


    return train_ds, val_ds


def test_model():
    # Load the model
    model = tf.keras.models.load_model("cnn-scratch.h5")

    imageDir = "../AF_dataset"
    classes = sorted(os.listdir(imageDir))
    print(classes)
    class_dict = {k: v for k, v in enumerate(classes)}

    ground_truth = []
    inferred = []

    print(class_dict)
    # File directory for test image
    path = '../AF_dataset/*/*.png'

    import imageio
    from PIL import Image
    incorrect = 0
    for img in glob.glob(path):
        # Read Image from  path
        pixels = Image.open(img)
        #print(img)
        # Convert the image to np array
        pixels = np.asarray(pixels)
        # Change the pixels to float
        pixels = pixels.astype('float32')
        # Expand the dimensions
        pixels = np.expand_dims(pixels, axis=0)
        # Predict the image
        prediction = model.predict(pixels)
        # Store the predicton probality for the test image
        prediction_probability = np.amax(prediction)
        # Store the index of the prediction probability (highest)
        prediction_idx = np.argmax(prediction)

        #print("Predicted artist =", class_dict[prediction_idx].replace('_', ' '))
        #print("Prediction probability =", prediction_probability * 100, "%")

        gt = img.split('\\')[1]


        # Store the index of the ground truth label
        ground_truth.append(classes.index(gt))

        # Store the index of the predicted label
        inferred.append(prediction_idx)



        inference = class_dict[prediction_idx]
        if gt != inference:
            print(gt, inference)
            incorrect += 1

    print("Infered: ", inferred)
    print("ground truth: ", ground_truth)
    confusion = metrics.confusion_matrix(ground_truth, inferred)
    print(confusion)
    print("Incorrect images =  ", incorrect)

    # Plot the confusion matrix

    df_cm = pd.DataFrame(confusion, index=[i for i in classes],
                         columns=[i for i in classes])
    plt.figure(figsize=(10, 7))
    sns.heatmap(df_cm, annot=True)
    plt.show()


def main():
    # Load the dataset
    #dataset = load_dataset(batch_size=32)
    # Train the Model
    #model = train_model(dataset=dataset,epochs=45)

    # Save the model
    #model.save("cnn-scratch.h5")

    # Test the Model
    test_model()

if __name__ == "__main__":
    print("Starting CNN")
    main()
    print("End of CNN")

