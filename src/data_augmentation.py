from PIL import Image
import glob

def flip(img, method):
    '''
    img: source image to be used
    method: numeric value for flipping direction
    0 - horizontal
    1 - vertical
    '''
    methods = {0:Image.FLIP_LEFT_RIGHT,
               1:Image.FLIP_TOP_BOTTOM}
    
    return img.transpose(method = methods[method])

def combined_flip(img):
    return flip(flip(img, 0), 1)

def rotate(img, i):
    return img.rotate(i * 90)

def save_augmentations(img_name):
    img = Image.open(img_name)
    img_name = img_name.split('.')[0]
    rotate(img, 1).save(img_name + 'r1.png')
    rotate(img, 2).save(img_name + 'r2.png')
    rotate(img, 3).save(img_name + 'r3.png')
    flip(img, 0).save(img_name + 'f0.png')
    rotate(flip(img, 0), 1).save(img_name + 'f1.png')
    rotate(flip(img, 0), 2).save(img_name + 'f2.png')
    rotate(flip(img, 0), 3).save(img_name + 'f3.png')

for image in glob.glob('AF_dataset/*/*.png'):
    save_augmentations(image)
