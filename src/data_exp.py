import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt

#read dataset
path = 'src/metadata_clean.csv'
df = pd.read_csv(path, na_values='-')

#correct the date entries such that they can be read
df['Date'] = df['Date'].str.extract(r'([0-9]{4})', expand=False)

#DATA EXPLORATION
fig, ax = plt.subplots(2,2, constrained_layout=True)

#FIRST SUBPLOT
ax[0,0].hist(df['Date'].sort_values().to_list(), bins=df['Date'].nunique(), rwidth=0.7)
text = "nan values represent the portion of the data that has no Date associated with it"
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
ax[0,0].text(0.05, 0.95, text, fontsize=14,
        verticalalignment='top', transform=ax[0,0].transAxes, bbox=props)
ax[0,0].set_title('Portrait dataset Date distribution')
ax[0,0].tick_params(axis='x', rotation=90)

#SECOND SUBPLOT
ax[0,1].hist(df['Style'].sort_values(), bins=df['Style'].nunique(), rwidth=0.5)
ax[0,1].set_title('Portrait dataset Style distribution')
ax[0,1].tick_params(axis='x', rotation=45)

#THIRD SUBPLOT
df2 = df.groupby(['Style', 'Artist']).size()
df2 = df2.groupby(['Style']).count()

df2.plot.bar(ax=ax[1,0])
ax[1,0].set_title('Frequencies of Artists/Style')
ax[1,0].tick_params(axis='x', rotation=45)

#FOURTH SUBPLOT
x = ['Dates', 'Titles', 'Unique Artists', 'Unique Styles', 'Entries/Artist']
y1 = [df.shape[0]-df['Date'].isna().sum(), df.shape[0]-df['Title'].isna().sum(), df['Artist'].nunique(), df['Style'].nunique(), len(df.index)//df['Artist'].nunique()]
y2 = [df['Date'].isna().sum(), df['Title'].isna().sum(), 0, 0, 0]

ax[1,1].bar(x, y1, 0.35)
ax[1,1].bar(x, y2, 0.35, bottom=y1, label='missing values (nan)')
ax[1,1].set_title('General Dataset Stats')
ax[1,1].legend()

ax[1,1].text('Dates', df.shape[0]-df['Date'].isna().sum() - 5, str(df.shape[0]-df['Date'].isna().sum()), color='black', fontweight='bold')
ax[1,1].text('Titles', df.shape[0]-df['Title'].isna().sum() - 5, str(df.shape[0]-df['Title'].isna().sum()), color='black', fontweight='bold')
ax[1,1].text('Dates', df['Date'].isna().sum() + 145, str(df['Date'].isna().sum()), color='black', fontweight='bold')
ax[1,1].text('Titles', df['Title'].isna().sum() + 160, str(df['Title'].isna().sum()), color='black', fontweight='bold')
ax[1,1].text('Unique Artists', df['Artist'].nunique() - 5, str(df['Artist'].nunique()), color='black', fontweight='bold')
ax[1,1].text('Unique Styles', df['Style'].nunique() - 5, str(df['Style'].nunique()), color='black', fontweight='bold')
ax[1,1].text('Entries/Artist', len(df.index)//df['Artist'].nunique() - 5, str(len(df.index)//df['Artist'].nunique()), color='black', fontweight='bold')

plt.show()


